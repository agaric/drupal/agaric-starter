if ENV["ENVIRONMENT"] == "dev" then

  desc "Copy database and files from stage and run updates locally."
  task :sync_from_stage_and_update => [:sync_from_stage, :update_local]

  desc "Copy database and files from stage site to local."
  task :sync_from_stage do
    sh "./vendor/bin/drush sql-dump > /tmp/paranoia.sql"
    sh "./vendor/bin/drush -y sql-sync @stage @self"
    sh "./vendor/bin/drush cr" # Needed after sql-sync for drush -y cex to work right away.
    sh "./vendor/bin/drush -y rsync @stage:%files/ @self:%files"
  end

  desc "Set admin/admin user/pass."
  task :set_admin_login do
    sh "./vendor/bin/drush user-password madmin --password=madmin"
  end

end
